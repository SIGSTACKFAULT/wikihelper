#!/usr/bin/python3

from pycurl import version_info as pycurl_version_info
from zlib import ZLIB_VERSION

import discord
from discord.ext import commands

import settings
import parse


bot = commands.Bot(command_prefix="^")

@bot.listen()
async def on_message(M):
	await parse.parse(M)

@bot.command()
async def about(ctx):
	em = discord.Embed(
		title = f"{settings.WHOAMI}/{settings.VERSION}",
		description = f"*\"{settings.VERSIONSTRING}\"*",
		url = settings.URL,
		colour = 0x7289da
	)
	for lib,ver in settings.LIBS:
		em.add_field(name=lib,value=ver)
	 
	await ctx.send("", embed=em)

@bot.command()
async def agent(ctx):
	await ctx.send(f"```{settings.USERAGENT}```")

with open("token", "r") as f:
	bot.run(f.read().strip())
