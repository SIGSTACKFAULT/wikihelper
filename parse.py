import itertools
import settings, try_url


start = "[["
end = "]]"

def xyzzy(s, start, end):
	finds = []
	for i, c in enumerate(s):
		if(s[i:i+len(start)] == start):
			find = ""
			after = s[i+2:]
			for i2, c2 in enumerate(after):
				i+=1
				if(after[i2:i2+len(end)] == end):
					break
				find = find + c2
			if(find):
				finds.append(find)
				find_title = find.title()
				if(find_title != find):
					finds.append(find_title)
	return finds

def handle_find(find, pages=[], errs=[]):
		find = find.replace(" ", "_")
		opts, find = parse_find(find)
		url = settings.WIKI.format(page=find)
		try:
			code = try_url.try_url(url, opts=opts)
		except try_url.pycurl.error as e:
			errs.append((repr(find),e.args[1]))
			return pages, errs
		except:
			errs.append((repr(find),"<Unknown Error: {}>".format(str(e))))
		else:
			if(code == 200):
				pages.append(url)
		return pages, errs


async def parse(M):
	finds = xyzzy(M.content, start, end)
	if(finds == []): return
	
	pages = []
	errs = []
	for find in finds:
		pages, errs = handle_find(find, pages, errs)
	
	for pair in itertools.combinations(pages, 2):
		pass
	
	if(pages):
		pages = list(set(pages))
		await M.channel.send(" | ".join(pages))
	if(errs):
		errs = [("`"+e[0][1:len(e[0])-1]+"`",e[1]) for e in errs]
		await M.channel.send("\n".join(["{e[0]}: {e[1]}".format(e=e) for e in errs]))
	try_url.cache.cache.save()

def parse_find(find):
	opts = {}
	opts["follow"] = True
	if(find[0] == "*"):
		opts["nocache"] = True
		find = find[1:]
	
	return opts, find
