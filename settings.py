import pycurl, zlib, discord

WHOAMI="Wikihelper"
URL="https://gitlab.com/Blacksilver/wikihelper"
VERSION="1.0.1"
VERSIONSTRING="Stableish:tm:"
PREFIX="&"
USERAGENT = f"{WHOAMI}/{VERSION} ({pycurl.version})"
CACHEFILE = "cache.json"
ZIP_CACHE = True
ZIP_CACHEFILE = "cache.z"

WIKI = "https://wikipedia.org/wiki/{page}"
OKCODE = 200

LIBS=[
	("pycurl", pycurl.version_info()[1]),
	("zlib", zlib.ZLIB_VERSION)
]
